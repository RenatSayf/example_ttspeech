package com.example.renat.example_ttspeech;

import android.os.Build;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
{
    private TextView textViewEn;
    private TextView textViewRu;
    private Button buttonEn;
    private Button buttonRu;
    private Button buttonNext;
    private Chronometer chronometer;
    private TextToSpeech speech;
    private HashMap<String, String> map = new HashMap<String, String>();
    private ArrayList<Words> wordses;
    private Locale localeEn = Locale.US;
    private Locale localeRu = Locale.getDefault();
    private String logTag = "TTS";
    private int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        speech = new TextToSpeech(this, new TextToSpeech.OnInitListener()
        {
            @Override
            public void onInit(int status)
            {
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "text");
                if (status == TextToSpeech.SUCCESS)
                {
                    int resultRu = speech.isLanguageAvailable(localeRu);
                    if (resultRu == TextToSpeech.LANG_MISSING_DATA || resultRu == TextToSpeech.LANG_NOT_SUPPORTED)
                    {
                        Toast.makeText(MainActivity.this, localeRu.getDisplayName() + "язык не подерживается",Toast.LENGTH_SHORT).show();
                        buttonRu.setEnabled(false);
                    }

                    int resultEn = speech.isLanguageAvailable(localeEn);
                    if (resultEn == TextToSpeech.LANG_MISSING_DATA || resultEn == TextToSpeech.LANG_NOT_SUPPORTED)
                    {
                        Toast.makeText(MainActivity.this, localeEn.getDisplayName() + "язык не подерживается",Toast.LENGTH_SHORT).show();
                        buttonEn.setEnabled(false);
                    }
                }else
                {
                    Toast.makeText(MainActivity.this,"TextToSpeech - ошибка инициализации",Toast.LENGTH_SHORT).show();
                    buttonEn.setEnabled(false);
                    buttonRu.setEnabled(false);
                }
            }
        });

        speech.setOnUtteranceProgressListener(new UtteranceProgressListener()
        {
            @Override
            public void onStart(String s)
            {
                chronometer.stop();
            }

            @Override
            public void onDone(String s)
            {

            }

            @Override
            public void onError(String s)
            {

            }
        });
    }

    private void initViews()
    {
        wordses = new ArrayList<>();
        wordses.add(new Words("good", "хорошо"));
        wordses.add(new Words("red", "красный"));
        wordses.add(new Words("add", "добавить"));
        wordses.add(new Words("select", "выбрать"));

        textViewEn = (TextView) findViewById(R.id.txt_view_en);
        textViewRu = (TextView) findViewById(R.id.txt_view_ru);
        buttonEn = (Button) findViewById(R.id.btn_en);
        buttonRu = (Button) findViewById(R.id.btn_ru);
        buttonNext = (Button) findViewById(R.id.btn_next);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
        buttonEn_OnClick();
        buttonRu_OnClick();
        buttonNext_OnClick();

        textViewEn.setText(wordses.get(0).getEnglish());
        textViewRu.setText(wordses.get(0).getRussian());
    }

    private void buttonNext_OnClick()
    {
        buttonNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                count++;
                if (count > wordses.size() - 1)
                {
                    count = 0;
                }
                textViewEn.setText(wordses.get(count).getEnglish());
                textViewRu.setText(wordses.get(count).getRussian());
            }
        });
    }

    private void buttonRu_OnClick()
    {
        buttonRu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                speech.setLanguage(localeRu);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    speech.speak(textViewRu.getText().toString(), TextToSpeech.QUEUE_ADD, null, "xxx");
                }else
                {
                    speech.speak(textViewRu.getText().toString(),TextToSpeech.QUEUE_ADD, map);
                }
            }
        });
    }

    private void buttonEn_OnClick()
    {
        buttonEn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                speech.setLanguage(localeEn);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    speech.speak(textViewEn.getText().toString(),TextToSpeech.QUEUE_ADD, null, "xxx");
                } else
                {
                    speech.speak(textViewEn.getText().toString(),TextToSpeech.QUEUE_ADD, map);
                }
            }
        });
    }


}
