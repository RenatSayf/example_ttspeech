package com.example.renat.example_ttspeech;

/**
 * Created by Renat on 16.07.2016.
 */
public class Words
{
    private String english;
    private String russian;

    public Words(String english, String russian)
    {
        this.english = english;
        this.russian = russian;
    }

    public String getEnglish()
    {
        return english;
    }

    public void setEnglish(String english)
    {
        this.english = english;
    }

    public String getRussian()
    {
        return russian;
    }

    public void setRussian(String russian)
    {
        this.russian = russian;
    }
}
